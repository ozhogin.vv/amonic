# Generated by Django 4.0.5 on 2023-12-05 13:01

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(db_column='ID', primary_key=True, serialize=False)),
                ('name', models.CharField(db_column='Name', max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Countries',
                'db_table': 'countries',
            },
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(db_column='ID', primary_key=True, serialize=False)),
                ('title', models.CharField(db_column='Title', max_length=50)),
            ],
            options={
                'db_table': 'roles',
            },
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(db_column='ID', primary_key=True, serialize=False)),
                ('title', models.CharField(db_column='Title', max_length=50)),
                ('phone', models.CharField(db_column='Phone', max_length=50)),
                ('contact', models.CharField(db_column='Contact', max_length=250)),
                ('country_id', models.ForeignKey(db_column='CountryID', on_delete=django.db.models.deletion.DO_NOTHING, to='main.country')),
            ],
            options={
                'db_table': 'offices',
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('id', models.AutoField(db_column='ID', primary_key=True, serialize=False)),
                ('email', models.CharField(db_column='Email', max_length=150, unique=True)),
                ('password', models.CharField(db_column='Password', max_length=50)),
                ('first_name', models.CharField(db_column='FirstName', max_length=50)),
                ('last_name', models.CharField(db_column='LastName', max_length=50)),
                ('birthdate', models.DateField(db_column='Birthdate')),
                ('last_active', models.DateTimeField(db_column='LastActive', default=datetime.datetime(2023, 12, 5, 16, 1, 5, 726814))),
                ('is_active', models.BooleanField(db_column='Active', default=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.group', verbose_name='groups')),
                ('office', models.ForeignKey(db_column='OfficeID', default=1, on_delete=django.db.models.deletion.DO_NOTHING, related_name='office_id', to='main.office')),
                ('role', models.ForeignKey(db_column='RoleID', default=2, on_delete=django.db.models.deletion.DO_NOTHING, related_name='role_id', to='main.role')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.permission', verbose_name='user permissions')),
            ],
            options={
                'db_table': 'users',
            },
        ),
    ]
