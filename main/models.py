import jwt

from datetime import datetime, timedelta

from django.conf import settings

from django.contrib.auth.models import (
	AbstractBaseUser, BaseUserManager, PermissionsMixin
)

from django.db import models


class Country(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    name = models.CharField(db_column='Name', max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'countries'
        verbose_name_plural = 'Countries'


class Office(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    country_id = models.ForeignKey(Country, models.DO_NOTHING, db_column='CountryID')
    title = models.CharField(db_column='Title', max_length=50)
    phone = models.CharField(db_column='Phone', max_length=50)
    contact = models.CharField(db_column='Contact', max_length=250)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'offices'


class Role(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    title = models.CharField(db_column='Title', max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'roles'


class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, birthdate, office, password = None):
        if email is None:
            raise TypeError('Users must have an email address.')

        if first_name is None:
            raise TypeError('Users must have a first name.')

        if last_name is None:
            raise TypeError('Users must have a last name.')

        if birthdate is None:
            raise TypeError('Users must have a birthdate.')

        if office is None:
            raise TypeError('Users must have an office.')

        user = self.model(email=self.normalize_email(email), first_name=first_name, last_name=last_name, birthdate=birthdate, office=office)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, first_name, last_name, birthdate, office, password = None):
        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(email, first_name, last_name, birthdate, office, password)
        user.is_superuser = True
        user.is_staff = True
        user.role = 1
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):
    id = models.AutoField(db_column='ID', primary_key=True)
    email = models.CharField(db_column='Email', max_length=150, unique=True)
    password = models.CharField(db_column='Password', max_length=150)
    first_name = models.CharField(db_column='FirstName', max_length=50)
    last_name = models.CharField(db_column='LastName', max_length=50)
    birthdate = models.DateField(db_column='Birthdate')
    role = models.ForeignKey(Role, models.DO_NOTHING, db_column='RoleID', default=2, related_name='role_id')
    office = models.ForeignKey(Office, models.DO_NOTHING, db_column='OfficeID', default=1, related_name='office_id')
    last_active = models.DateTimeField(db_column='LastActive', default=datetime.now())
    is_active = models.BooleanField(db_column='Active', default=True)

    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'birthdate', 'office']

    objects = UserManager()

    def __str__(self):
        return self.email

    @property
    def token(self):
        return self._generate_jwt_token()

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def _generate_jwt_token(self):
        dt = datetime.now() + timedelta(days=1)

        token = jwt.encode({
            'id': self.pk,
            'exp': dt.utcfromtimestamp(dt.timestamp())
        }, settings.SECRET_KEY, algorithm='HS256')

        return token

    class Meta:
        db_table = 'users'


class Activity(models.Model):
    pass
