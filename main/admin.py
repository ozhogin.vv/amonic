from django.contrib import admin

from .models import User, Office, Role, Country


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'role_id')


class RoleAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')


class CountryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class OfficeAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')


admin.site.register(User, UserAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Office, OfficeAdmin)
