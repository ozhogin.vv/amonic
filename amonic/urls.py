from django.contrib import admin
from django.urls import path, include, re_path

from main.views import *

from rest_framework import routers


#router = routers.DefaultRouter()
#router.register(r'users', UserViewSet, basename='users')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/user/', UserRetrieveUpdateAPIView.as_view()),
    path('api/v1/users/', RegistrationAPIView.as_view()),
    path('api/v1/users/login/', LoginAPIView.as_view()),

    #path('api/v1/', include(router.urls)),
    #path('api/v1/auth/', include('djoser.urls')),
    #re_path(r'^auth/', include('djoser.urls.authtoken')),
]
