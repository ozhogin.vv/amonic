from datetime import datetime, timezone

from django.http import HttpResponse


class LastActiveMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = request.user
        if user.is_authenticated:
            last_active = user.last_active
            now = timezone.now()

            if last_active <= now - datetime.timedelta(minutes=3):
                user.last_active = now
                user.save()

        response = self.get_response(request)

        return response
